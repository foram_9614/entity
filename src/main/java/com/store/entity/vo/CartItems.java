package com.store.entity.vo;

public class CartItems {
	
	int productId;
	int quantity;
	String productName;
	double productPrice;
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	@Override
	public String toString() {
		return "OrderItems [productId=" + productId + ", quantity=" + quantity + ", productName=" + productName
				+ ", productPrice=" + productPrice + "]";
	}
	

}
